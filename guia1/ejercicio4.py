#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def dibuja(altura=0, ancho=0):

    for i in range(altura):
        text = ""
        for j in range(ancho):
            text += "*"
        print(text)


# Main
while True:
    try:
        print("Ingrese: ")
        a, b = (int(input("La altura: ")),
                int(input("El ancho: ")))
        dibuja(altura=a, ancho=b)
        break
    except ValueError:
        print("Ingreso un número no válido")
