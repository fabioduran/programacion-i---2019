#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Realizar una función que, dada una lista, devuelva una nueva lista cuyo
contenido sea igual a la original pero invertida. Ası́, dada la lista:
[’Di’, ’buen’, ’dı́a’, ’a’, ’papa’]
Devuelve
[’papa’, ’a’, ’dı́a’, ’buen’, ’Di’].
"""


def invierte_lista(lista=None):

    nueva_lista = []
    n = len(lista)
    for i in range(len(lista)):
        nueva_lista.append(lista[n-1])
        n -= 1
    return nueva_lista


# Main
lista = ["Di", "buen", "día", "a", "papá"]
n_lista = invierte_lista(lista)
print("La lista original: ", lista)
print("La nueva lista: ", n_lista)
