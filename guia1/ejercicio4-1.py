#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Escriba un programa que pida al usuario ingresar la altura y el ancho de un rectángulo y lo dibuje
utilizando asteriscos:

Altura: ‘3‘
Ancho: ‘5‘
*****
*****
*****
"""


def dibuja(altura=0, ancho=0):

    for i in range(altura):
        # multiplica n veces el string
        print("*"*ancho)


# Main
while True:
    try:
        print("Ingrese: ")
        a, b = (int(input("La altura: ")),
                int(input("El ancho: ")))
        dibuja(altura=a, ancho=b)
        break
    except ValueError:
        print("Ingreso un número no válido")
