#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Cuando un huevo es hervido en agua, las proteı́nas comienzan a coagularse
cuando la temperatura sobrepasa un punto crı́tico. A medida que la
temperatura aumenta, las reacciones se aceleran.
En la clara, las proteı́nas comienzan a coagularse para temperaturas sobre 63C,
mientras que en la yema lo hacen para temperaturas sobre 70C. Para hacer un
huevo a la copa, la clara debe haber sido calentada lo suficiente para
coagularse a más de 63 ◦ C, pero la yema no debe sobrepasar los 70C para
evitar obtener un huevo duro.
"""

from math import pi, log


def huevo_copa(t0=0):

    M = 67  # Masa. Asumiremos un huevo grande
    c = 3.7  # Capacidad calorifica especifica
    ro = 1.038  # Densidad
    K = 0.0054  # Conductividad termica
    tw = 100  # Temperatura de ebullicion del agua
    ty = 70  # Temperatura final

    # Ahora, calculamos el tiempo necesario para llegar a la temperatura final
    t = ((M**(2/3) * c * ro**(1/3)) /
         (K * pi**2 * ((4 * pi)/3)**(2/3)) *
         log(0.76 * ((t0 - tw)/(ty - tw)))
         )
    print("El tiempo necesario es "+str(t)+" s")


# Main
while True:
    try:
        t0 = float(input("Ingrese la temperatura original del huevo: "))
        huevo_copa(t0)
        break
    except ValueError:
        print("No es un número válido")
