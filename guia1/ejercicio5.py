#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Al recibir como dato un número entero N , obtenga el resultado de la siguiente
serie: 1^1 - 2^2 + 3^3 - 4^4 ... + N^N
Dato: N (variable de tipo entero que representa el número de términos de la
serie).
"""


def serie(numero=0):

    suma = 0

    # en rango de 1 hasta numero + 1
    for i in range(1, numero+1):
        if i % 2 == 0:
            suma -= i ** i
        else:
            suma += i ** i
    return suma


# Main
while True:
    try:
        numero = int(input("Ingrese un número"))
        print(serie(numero))
        break
    except ValueError:
        print("No ha ingresado un número válido")
