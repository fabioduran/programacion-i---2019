#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Escribir una función que indique si dos fichas de dominó encajan o no.
Las fichas son recibidas en arreglo (tuplas), por ejemplo: (3,4) y (5,4).
"""


def encajan(tupla1, tupla2):
    """
    Evalua cada posicion de tupla 1 y si esta corresponde con alguna posicion
    de tupla 2 determina correspondencia
    """
    for pos in tupla1:
        if pos == tupla2[0] or pos == tupla2[1]:
            return "Las fichas encajan"
        return "Las fichas no encajan"


def separar(cadena1, cadena2):
    """
    Separa las cadenas en tuplas cada una y si las envia para ser evauladas en
    la funcion encajan.
    """
    # FIXME - corregir el ingreso
    cad1 = cadena1.split('-')
    cad2 = cadena2.split("-")
    return encajan(cad1, cad2)


print("Ingrese de la forma 2-3")
cadena1 = input("Ingrese la ficha 1: ")
cadena2 = input("Ingrese la ficha 2: ")

print(separar(cadena1, cadena2))
