#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Escriba un programa que pregunte al usuario la hora actual t del reloj y un
número entero de horas h, que indique qué hora marcará el reloj dentro de h
horas:
"""


def error():
    print("No ha ingresado un número válido")


def operacion(hora=0, incremento=0):
    """
    Resuelve el resultado de modo ciclico, usando la condición de nueva_hora
    mayor que 24, restando en 24 a ese resultado cuando cumpla la condición.
    La operación está en formato de 24 horas.
    """
    nueva_hora = hora + incremento
    while nueva_hora > 24:
        nueva_hora -= 24

    return nueva_hora


# Main
try:
    print("Ingrese:")
    hora, incremento = (int(input("La hora actual: ")),
                        int(input("Ingrese un número para incrementar: "))
                        )
    n = operacion(hora, incremento)
    print("En {0} horas, el reloj marcará las {1}".format(incremento, n))
    # la salida anterior es equivalente a
    # print ("En", incremento, "horas, el reloj marcará las", n)

except TypeError:
    error()
except ValueError:
    error()
