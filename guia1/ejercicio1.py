#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Escriba un programa que pregunte al usuario la hora actual t del reloj y un
número entero de horas h, que indique qué hora marcará el reloj dentro de h
horas:
"""


def error():
    print("No ha ingresado un número válido")


def operacion(hora, incremento):
    """
    retorna el mod de 12 para visualizar la hora en formato 12 horas
    caso de usar el formato de 24 horas, el mod es a 24
    """
    nueva_hora = (hora + incremento) % 12

    return nueva_hora


try:
    print("Ingrese:")
    hora, incremento = (int(input("La hora actual: ")),
                        int(input("Ingrese un número para incrementar: ")))
    n = operacion(hora, incremento)
    print("En {0} horas, el reloj marcará las {1}".format(incremento, n))
    # la salida anterior es equivalente a
    # print ("En", incremento, "horas, el reloj marcará las", n)
except TypeError:
    error()
except ValueError:
    error()
