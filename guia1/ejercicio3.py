#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Un partido de tenis en grand Slam se juega al mejor de cinco sets para el caso
que sea un partido de varones y al mejor de tres sets en damas. Para ganar un
set, un jugador debe ganar 6 juegos, pero además debe haber ganado por lo menos
dos juegos más que su rival. Si el set está empatado a 5 juegos,
el ganador es el primero que llegue a 7. Si el set está empatado a 6 juegos,
el set se define en un último juego, en cuyo caso el resultado final es 7-6.
Cree un Python para determinar un marcador de tenis y determinar un ganador.
Es inválido si:
Si existen dos jugadores del mismo sexo.
el resultado es 8-6 o 7-3.
"""


def juego_tenis(juegos_a=0, juegos_b=0):
    # Evaluamos condiciones del juego
    if (juegos_a >= 0 or juegos_b >= 0) and (juegos_a <= 7 and juegos_b <= 7):
        if juegos_a == 7 or juegos_b == 7:
            if (juegos_a - juegos_b) <= 2:
                if juegos_a == 7:
                    print("Gana A")
                else:
                    print("Gana B")
            else:
                print("Invalido")
        elif juegos_a == 6 or juegos_b == 6:
            if (juegos_a - juegos_b) >= 2:
                if juegos_a == 6:
                    print("Gana A")
                else:
                    print("Gana B")
            else:
                print("Aun no termina")
        else:
            print("Aun no termina")
    else:
        print("Invalido")


# Main
while True:
    try:
        juegos_a = int(input("Juegos ganados por A: "))
        juegos_b = int(input("Juegos ganados por B: "))
        juego_tenis(juegos_a, juegos_b)
        break
    except ValueError:
        print("Ingresó un número inválido")
