#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Escriba un programa que indique si un año es bisiesto o no, teniendo en cuenta
cuál era el calendario vigente en ese año:

El algoritmo del bisiesto es el siguiente
SI ((año divisible por 4) Y
    ((año no divisible por 100) O
    (año divisible por 400))) ENTONCES
    es bisiesto
SINO
    no es bisiesto
"""


def imprime_bisiesto(anio=0):
    texto = "El año {0} es ".format(anio)
    if anio % 4 == 0 and (anio % 100 != 0 or anio % 400 == 0):
        # concatena texto
        texto += "bisiesto"
    else:
        # concatena texto
        texto += "no bisiesto"
    print(texto)


# Main
while True:
    try:
        anio = int(input("Ingrese un año: "))
        imprime_bisiesto(anio)
        break
    except ValueError:
        print("No es un número válido, ingrese nuevamente")
