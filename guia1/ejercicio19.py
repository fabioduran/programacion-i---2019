#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def quitar_repetidos(lista=None):

    lista_nueva = []
    for i in lista:
        if i not in lista_nueva:
            lista_nueva.append(i)
    return lista_nueva


# Main
lista = [1, 1, 2, 3, 4, 4, 5]
nueva = quitar_repetidos(lista)
print(nueva)
