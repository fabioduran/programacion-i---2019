#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
Cambiar las key por values y values por key del siguiente diccionario:
# amino - # formula molecular
dic = { ’ Histidine ’: ’ C6H9N3O2 ’}
Debe quedar del siguiente modo:
# amino - # f molecular
nuevo = { 'C6': 'Histidine',
          'H9': 'Histidine',
          'N3': 'Histidine',
          'O2': 'Histidine'}
"""
def invierte_diccionario(dic):
    """
        Función que invierte key por values y segmenta el value en multiples
        keys de un diccionario.

        Recibe: Diccionario
    """
    temp = {}
    for key, value in dic.items():
        i = 0
        while i < len(value):
            temp[value[i:i+2]] = key
            i = i + 2

        # Esto simula esto
        # temp[value[0:2]] = key
        # temp[value[2:4]] = key
        # temp[value[4:6]] = key
        # temp[value[6:8]] = key

    print(temp)


if __name__ == "__main__":

    DIC = {'Histidine':'C6H9N3O2'}
    invierte_diccionario(DIC)
