#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 Se define una tupla global, esto quiere decir que cada elemento de la tupla
 contiene un valor partiendo desde cero. Esto representa a cada una de las
 columnas del dataset gracias a la función range().
 Así facilita el proceso de manejo de indices.
 Puede ver más claro esto usando un print(PROVINCIA) que devolverá un 3
 """
(SNO,
 FECHA,
 PROVINCIA,
 PAIS,
 ULT_ACTUALIZACION,
 CONFIRMADOS,
 MUERTOS,
 RECUPERADOS) = range(8)


def open_file():
    """
        Función que abre el archivo requerido
    """
    # se define un la lista vacia para rellenar en ella cada una de las
    # lineas del archivo.
    lineas = []
    with open("covid_19_data.csv") as file_:
        for line in file_:
            # segun el dataset, el delimitados de cada columna es
            # una coma, es por ello que la función split.
            # cada linea se vuelve una lista de N elementos, según la
            # cantidad de columnas
            lineas.append(line.split(","))
    return lineas


def cifras_por_pais(dic, linea, inc):
    """
        Función que procesa cada linea del archivo y las agrega a una
        llave (key) de diccionario.
        Es complemento de la función procesa_datos

        Recibe:
            dic: El diccionario que es procesado.
            linea: Linea actual que se está leyendo
            inc: sera cero cuando la linea contenga 8 columnas y uno cuando
                 la linea contenga 9 columnas.
        Devuelve:
            Diccionario: con la actualización de la llave(key) y su cantidad
                         actualizada.
    """

    # busca si existe elemento
    temp = dic.get(linea[PAIS + inc], False)

    # si el pais existe, incrementa sus valores
    if temp:
        confirmados = temp['confirmados'] + float(linea[CONFIRMADOS + inc])
        muertos = temp['muertos'] + float(linea[MUERTOS + inc])
        recuperados = temp['recuperados'] + float(linea[RECUPERADOS + inc])
    # no existe el pais
    else:
        temp = {}
        confirmados = float(linea[CONFIRMADOS + inc])
        muertos = float(linea[MUERTOS + inc])
        recuperados = float(linea[RECUPERADOS + inc])

    # segun estructura, agrega valores a un diccionario temp
    temp['confirmados'] = confirmados
    temp['muertos'] = muertos
    temp['recuperados'] = recuperados

    # se asigna el valor key pais el diccionario temporal
    dic[linea[PAIS + inc]] = temp

    return dic


def procesa_datos(datos):
    """
        Función que genera un diccionario procesado a fin visualizar la data.
        Hay que considerar que cada una de las lineas contiene filas, y que
        esas fila están delimitadas por comas. La dificultad de ello es que
        en la columna provincia en algunos casos existen comas,
        por lo tanto van a existir lineas con 8 y otras con 9 columnas

        El diccionario tiene la siguiente estructura:
        dic = {'pais': {'confirmados': cantidad,
                        'muertos': cantidad},
                        'recuperados': cantidad}

        Recibe: Datos, texto del archivo procesado en listas. Cada linea es
        una lista

        Devuelve: diccionario de todos los paises y sus resultados
    """
    dic = {}
    for i, linea in enumerate(datos):
        print(i, linea)
        # Omite linea de titulos de dataset
        if i == 0:
            continue
        # si no existe comas en la columnas de provincia
        if len(linea) == 8:
            # llama a función para agregar linea a diccionario
            dic = cifras_por_pais(dic, linea, 0)
        # Existe una coma en la linea de provincia
        elif len(linea) == 9:
            dic = cifras_por_pais(dic, linea, 1)
    return dic


def muestra_datos(datos):
    """
        Función que muestra los datos ya procesados

        Recibe: datos, que es un diccionario
    """
    for key, val in datos.items():
        if val['confirmados'] > 0:
            print("{0} tiene:".format(key))
            print("\t{0} confirmados".format(val['confirmados']))
            print("\t{0} recuperados".format(val['recuperados']))
            print("\t{0} muertos".format(val['muertos']))


if __name__ == "__main__":

    datos = open_file()
    datos = procesa_datos(datos)
    muestra_datos(datos)
