#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
0 Naturales: agua, leche, jugos, mostos, infusiones,
1 Gaseosas
2 Vinos
3 Cervezas
4 Whisky
5 Licores Varios: Amaretto, Operol
6 Energéticas: Red Bull
7 Combinados: Piscola, Tom Collins.
"""


import random


def ingreso_consumo(num_pueblo, num_tipo_bebida):
    """
        función para llenar de manera dinámica
        
        recibe:
            num_pueblo: número de dim y
            num_tipo_bebidas: número de dim x
            
        devuelve:
            res: matriz con datos
    """
    datos = []
    for i in range(num_pueblo):
        datos.append([])
        for j in range(num_tipo_bebida):
            litros = random.randint(0, 100)
            datos[i].append(litros)
    return datos


def bebida_mas_consumida(datos, num_tipo_bebida):

    """
        Función para conocer las bebidas más consumidas
        
        recibe:
            matriz con datos
            num_tipo_bebida: entero con el número total de bebidas
            
         retorna:
            res: lista con resultado
    """
    # se inicializa lista
    consumidos = []
    for i in range(num_tipo_bebida):
        consumidos.append(0)

    print(consumidos)
    for i in datos:
        for j in range(num_tipo_bebida):
            consumidos[j] = consumidos[j] + i[j]

    print(consumidos)

    res = determina_mayor_menor(mayor=True, datos=consumidos)
    return res


def bebidas_alcohol(alcohol):

    """
        Función para conocer las bebidas con mayor y menor alcohol
        
        recibe:
            lista con grados alcoholicos
    """

    res = determina_mayor_menor(datos=alcohol, mayor=True, menor=True)
    print("La bebida con más alcohol es {0} con {1}".format(res[1], res[0]))
    print("La bebida con menos alcohol es {0} con {1}".format(res[3], res[2]))


def comuna_consumo(datos):
    """
        determina los consumos por ciudad
        
        recibe: 
            datos: matriz
            
        devuelve:
            lista: suma del consumo de las ciudades
    """
    ciudad = []
    for i in range(10):
        ciudad.append(0)

    for i, dato in enumerate(datos):
        for n, valor in enumerate(dato):
            ciudad[i] = ciudad[i] + valor

    return ciudad


def jovenes_alcohol(datos, alcohol):
    """
        Función que determina y responde a cantidad de jovenes
        con mayor consumo de alcohol y además de la ciudad.
        
        recibe:
            matriz con datos
            alcohol: lista con los valores de grados de alcohol.
    """
    for i in datos:
        for n, valor in enumerate(i):
            if alcohol[n] == 0:
                i[n] = 0
    res = bebida_mas_consumida(datos, 8)
    bebida = res[1]
    litros = res[0]
    print("La bebida con alcohol más consumida es {0} con {1} litros".format(bebida,
                                                                             litros))

    res = comuna_consumo(datos)
    mayor = 0
    for i, ciudad in enumerate(res):
        if ciudad > mayor:
            mayor = ciudad
            indice = i

    print("La ciudad con mayor consumo es {0} con {1} litros".format(indice,
                                                                     mayor))


def determina_mayor_menor(datos, mayor=False, menor=False):
"""
    Función que permite determinar mayor y menor dentro de una matriz
    recibe:
            datos -> matriz
            
     devuelve:
            lista con mayor y su indice, menor y su indice
"""
    valor_mayor = None
    valor_menor = None
    indice_mayor = None
    indice_menor = None
    # no uso funciones max y min de las listas
    for i, dato in enumerate(datos):
        if mayor:
            if valor_mayor is None:
                valor_mayor = dato
                indice_mayor = i
            else:
                if dato > valor_mayor:
                    valor_mayor = dato
                    indice_mayor = i
        if menor:
            if valor_menor is None:
                valor_menor = dato
                indice_menor = i
            else:
                if dato < valor_menor:
                    valor_menor = dato
                    indice_menor = i

    return [valor_mayor, indice_mayor, valor_menor, indice_menor]


if __name__ == "__main__":

    # ejemplo muestra
    """
    datos = [[39, 48, 16, 68, 86, 57, 89, 25],
             [79, 71, 51, 18, 24, 94, 27, 27],
             [81, 49, 86, 60, 46, 57, 61, 100],
             [30, 62, 27, 74, 10, 98, 73, 93],
             [84, 69, 71, 73, 49, 29, 76, 13],
             [39, 3, 47, 97, 95, 6, 47, 63],
             [18, 18, 61, 3, 72, 43, 51, 85],
             [4, 50, 51, 75, 69, 31, 28, 63],
             [97, 28, 10, 62, 55, 6, 42, 49],
             [56, 32, 33, 96, 84, 66, 77, 99]]
    """
    num_bebidas = 8
    num_pueblos = 10
    datos = ingreso_consumo(num_pueblos, num_bebidas)
    alcohol_bebidas = [0, 0, 13.5, 5, 40, 12, 0, 30]
    res = bebida_mas_consumida(datos, 8)
    bebida = res[1]
    litros = res[0]
    print("La bebida más consumida es {0} con {1} litros".format(bebida,
                                                                 litros))
    bebidas_alcohol(alcohol_bebidas)
    res = jovenes_alcohol(datos, alcohol_bebidas)
