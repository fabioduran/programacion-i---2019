#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def menu(participantes=0, votos=[]):
    """
    Menú de la aplicación
    """
    print("Ingrese una opción")
    print("1. Ingrese un voto")
    print("2. Resultados")
    print("Cualquier otra tecla para salir")

    opt = input("Ingrese una opción: ")

    # Define el total de canciones
    total_canciones = 10

    # Opciones de menú
    if opt == "1":
        ingreso_votos(participantes=participantes, votos=votos)
    elif opt == "2":
        cuenta_votos(participantes, votos, total_canciones)
    else:
        quit()


def canciones_mas_votadas(lista):
    """
    Función que procesa las canciones más votadas

    Parámetros:
        recibe: lista que contiene la lista del total del votos de cada canción
            la lista tiene el siguiente formato:
            * la canción corresponde a la posición dentro del arreglo
            * el total corresponde a la suma de votos para esa canción
            lista = [1, 5, 6, ...]
            quiere decir que la canción 1 tiene un voto,
                    la canción dos 5 y la tercera 6

        devuelve: una lista con la posición de las dos canciones más votadas
                  del modo [[posición, canción][posición, canción]]
    """

    print("Las canciones más votadas son: ")

    # canción más votada (valor)
    max_uno = 0
    # segunda canción más votada (valor)
    max_dos = 0

    # ciclo que busca valores más altos
    for i in lista:
        if max_uno < i:
            max_dos = max_uno
            max_uno = i
        else:
            if max_dos < i:
                max_dos = i

    lugar = 1
    mas_votadas = []
    # recorre los dos valores
    for i in (max_uno, max_dos):
        indexPos = 0
        while True:
            try:
                # Busca por item la cantidad de posiciones que corresponde a
                # las canciones más votadas
                indexPos = lista.index(i, indexPos)
                indexPos = indexPos + 1
                msj = "En el lugar {0}, está la canción {1}".format(lugar,
                                                                    indexPos)
                print(msj)
                # lista con posición votación y canción a la que corresponde
                mas_votadas.append([lugar, indexPos])

            except ValueError:
                break
        lugar = lugar + 1

    return mas_votadas


def participantes_ganadores(mas_votadas, votos):

    """
    Función que determina posiciones y ganadores del concurso

    recibe: mas_votadas: lista con posicion y canción mas votada
            votos: lista con participantes y votos emitidos
    """

    # recorre todas la votaciones de todos los participantes
    for i in votos:
        # variables necesarias
        participante = i[0]
        canciones = i[1]
        acerto = 0
        puntaje = 0

        # recorre canciones más votadas
        for j in mas_votadas:
            # variables necesarias
            lugar = j[0]
            cancion = j[1]
            # Si se encuentra en primer lugar
            if cancion in canciones and lugar == 1:
                puntaje = puntaje + 30
                acerto = acerto + 1

            # se encuentra en segundo lugar
            if cancion in canciones and lugar == 2:
                puntaje = puntaje + 20
                acerto = acerto + 1

            # acertó dos veces, aplica puntaje adicional
            if acerto > 1:
                puntaje = puntaje + 10

        print("El participante {0} obtuvo {1}".format(participante,
                                                      puntaje))
        puntaje = 0


def cuenta_votos(participantes=0, votos=[], total_canciones=10):

    """
    Función que cuenta los votos emitidos
    recibe: * participante: cantidad de participantes
            * votos: lista con todos los votos emitidos del modo
                votos=[[1, [1, 2, 3]], [2, [1, 2, 3]]
                una lista dentro de otra con el primer elemento que representa
                al participante y el segundo elemento es una lista con las
                canciones seleccionadas.
            * total_canciones: el total de canciones a votar
    """

    # Si no hay votos, vuelve al menú
    if not votos:
        print("No hay votos emitidos\n")
        menu()

    # existen votos
    canciones_votos = []
    for i in range(total_canciones):
        cuenta = 0
        for j in votos:
            canciones_votadas = j[1]
            # el i+1 es porque los indices comienzan en cero
            # si se encuentra la canción se suma un voto
            if i+1 in canciones_votadas:
                cuenta = cuenta + 1
        print("La canción {0} tiene {1}".format(i+1, cuenta))
        canciones_votos.append(cuenta)
    print(canciones_votos)

    # llama a funciones para obtener calculos y salidas de resultados
    mas_votadas = canciones_mas_votadas(canciones_votos)
    participantes_ganadores(mas_votadas, votos)


def ingreso_votos(participantes, votos):

    """
    Función que permite el ingreso de los votos y determina los
    participantes disponibles
    Recibe * participantes: el numero de participantes con votación realizada
            * votos: lista con los votos hasta el momento emitidos
    """
    participantes = participantes + 1
    print("Bienvenido participante {0}".format(participantes))

    temp = []
    # Son 3 las canciones que puede un participante seleccionar
    for i in range(0, 3):
        # evita que se ingrese la misma opción dos veces.
        while True:
            v = int(input("Ingrese una votacion {0}: ".format(i+1)))
            if v in temp:
                print("La canción {0} ya fue votada".format(v))
                continue
            if 1 <= v <= 10:
                break
        temp.append(v)
    votos.append([participantes, temp])

    print(votos)
    print("""
            Presione enter para ingresar otro participante o
            cualquier otra tecla para volver a menu
          """)
    opt = input()
    if opt == "":
        # recursivo
        ingreso_votos(participantes, votos)
    else:
        menu(participantes, votos)


# Main
if __name__ == "__main__":
    print("Concurso de Radio \n\n\n")
    menu()
