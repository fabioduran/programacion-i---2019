#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import random
import copy
import itertools
import time

UNIX = True  # Change if using Windows


def clear():
    """
    Función utilitaria para limpiar pantalla
    """
    return os.system('clear') if UNIX else os.system('cls')


def random_life():
    """
    Esta función genera para cada columna valores entre 0 y 1
    Se considera como True (1) o False (0)

    Retorna:
        Una lista de valores para una columna con valores entre 1 y 0
    """

    return [random.randint(0, 1) for n in range(cols)]


def search_in_matrix(x, y):
    """
    Retorna el valor Booleano de la posicion en una matriz,
    Este recibe como parámetros los valores posibles para una Matriz,
    respecto a la permutación de valores posibles. Con esto evitamos
    que salga de los parámetros de la matriz

    returna:
        x: valor entero
        y: valor entero
    """

    return (x in range(rows) and y in range(cols))


def start_game(rows, cols):
    """
    Rellena los valores de la matriz.
    Esta es la primera iteración para la matriz.

    Recibe:
        rows: número entero que entrega el número máximo de la fila
        cols: número entero que entrega el número máximo de la columna

    Retorna:
        game: Una matriz con las celulas de forma aleatorias entre 0 y 1
    """

    game = [random_life() for n in range(rows)]
    return game


def evaluate_matrix(game, row, col):
    """
    Función que evalúa todas las posiciones posibles para una célula (vecinos).
    Se restringe a las posiciones de la permutación.

    Recibe:
        game: matriz con celulas a analizar
        row: La posición de la fila de la matriz a analizar
        col: La posición de la columna de la matriz a analizar

    Retorna:
        Total: Entero que cuenta el total de los elementos encontrados
               dentro de la área establecida en la permutación,
               (búsqueda de vecinos)
    """

    # Genera una lista con la permutación de las posiciones posibles
    distance = list(set(itertools.permutations([-1, -1, 1, 1, 0], 2)))
    # print(distance)
    total = 0

    for r, c in distance:
        # print (r, c)
        # print(search_in_matrix(r+row, c+col))
        if search_in_matrix(r+row, c+col):
            total += game[r + row][c + col]
        # print(total)
    return total


def play(game, rows, cols):
    """
    Función principal que permite la evaluación y continuidad del
    juego, acá cuenta y analiza todas las posibles celulas y determina
    la vida o muerte de la misma.

    Recibe:
        game: matriz actual del juego
        rows: total de filas
        cols: total de columnas

    Retorna:
        game: Una nueva matriz con nuevos valores de las células
        life: contador de células vivas
        death: contador de células muertas
    """

    gameaux = copy.deepcopy(game)
    life = 0
    dead = 0

    for row in range(rows):
        for col in range(cols):
            total = evaluate_matrix(game, row, col)

            if (total < 2 or total > 3) and gameaux[row][col]:
                gameaux[row][col] = 0
                dead += 1
            elif total == 3 and not gameaux[row][col]:
                gameaux[row][col] = 1
                life += 1

    game = copy.deepcopy(gameaux)
    return game, life, dead


def print_matrix(game, life, dead):
    """
    Función que imprime la matriz y además de información respecto al
    comportamiento de la matriz para células vivas y muertas

    Recibe:
        game: Una nueva matriz con los valores de las células
        life: contador de células vivas
        death: contador de células muertas
    """

    table = ''
    table += "Celulas Vivas: {0} Muertas: {1}\n".format(life, dead)
    for row in game:
        for cell in row:
            table += 'X ' if cell else '. '
        table += '\n'
    print(table)


if __name__ == "__main__":
    """
    Función principal o main
    """
    rows = 12
    cols = 12
    iterations = 0
    run = True

    game = start_game(rows, cols)
    while run:
        try:
            clear()
            print("Programación I")
            print("Juego de la vida de Conway")
            print("Matriz de {0} x {1}\n".format(rows, cols))
            game, life, dead = play(game, rows, cols)
            print_matrix(game, life, dead)
            if life == 0 and dead == 0:
                run = False
            time.sleep(1)
            iterations += 1
        except KeyboardInterrupt:
            break
print("Total: ", iterations)
