#!/usr/bin/env python3
#! -*- coding:utf-8 -*-


def alcubo(numero=0):
    return numero ** 3

def ingreso():
    while True:
        try:
            numero = int(input("Ingrese un número: "))

            if numero % 3 == 0:
                n = alcubo(numero)
                print("El número ingresado es ", numero)
                print("Elevado al cubo es, ", n)
            break
        except:
            print ("Ingrese el número nuevamente... ")

ingreso()
