#!/usr/bin/env python3
#! -*- coding:utf-8 -*-


def convertir(segundos=0):

    horas = int(segundos/3600)
    segundos -= 3600*horas

    minutos = int(segundos/60)
    segundos -= 60*minutos

    return horas, minutos, segundos

horas, minutos, segundos = convertir(3661)

print("Horas: ", horas)
print("Minutos: ", minutos)
print("Segundos:", segundos)
