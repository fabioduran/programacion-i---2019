#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

def imprimir_triangulo(ancho=0):

    for i in range(ancho):
        print("*" * (i+1))

while True:
    try:
        ancho = int(input("Ingrese ancho para triángulo: "))
        imprimir_triangulo(ancho)
        break
    except :
        print("Ha ingresado un caracter no numérico")
