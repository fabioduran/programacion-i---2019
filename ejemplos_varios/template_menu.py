#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import json

# Archivo tipo json basado en diccionarios
def guardar(estudiante):

    with open("estudiantes.txt", 'w') as file:
        json.dump(estudiante, file)


def cargar():

    with open("estudiantes.txt") as file:
        estudiante = json.load(file)

    return estudiante



# Otros archivos distintos de diccionario
def cargar_archivo():

    with open("covid_19_data.csv") as file:
        linea = file.readline()

        while linea != '':
            linea = linea.split(",")
            print(linea[3])
            linea = file.readline()


def guardar_archivo(data):

    with open("nuevo_archivo.txt", 'w') as file:
        file.write(data)

def insertar(estudiante):

    # validación
    nombre = input("Ingrese el nombre del estudiante: ")

    notas = []
    for i in range(0, 4):
        # validación
        nota = input("Ingrese nota {0}: ".format(i+1))
        notas.append(nota)

    estudiante[nombre] = notas

    guardar(estudiante=estudiante)

    return estudiante

def editar(estudiante):

    # key
    nombre = input("Ingrese el nombre del estudiante a editar: ")

    temp = estudiante.get(nombre, False)

    # if temp == "no encontrado":
    if not temp:
        print ("El estudiante no se ha encontrado")
    else:
        notas = []
        for i in range(0, 4):
            # validación
            nota = input("Ingrese nota {0}: ".format(i+1))
            notas.append(nota)
        estudiante[nombre] = notas

    return estudiante

def eliminar(estudiante):

    nombre = input("Ingrese el nombre del estudiante a eliminar: ")

    try:
        del estudiante[nombre]
    except :
       print("no se ha encontrado")


    # otra opción
    """
    temp = estudiante.get(nombre, False)
    if not temp:
        print("No encontrado")
    else:
        del estudiante[nombre]
    """

def buscar():
    pass

"""
{'gabriel': [6, 4.3, 3, 7],
 'Benjamin' [4, 5, 1, 7],
}

"""


def menu():

    estudiante = {}

    estudiante = cargar()

    print(estudiante)

    #return False

    while True:
        print("Presione:")
        print("b para buscar")
        print("i para insertar")
        print("e para editar")
        print("d para eliminar")
        print("s para salir")
        opcion = input("Ingrese una opción: ")

        if opcion.upper() == "B":
            buscar()

        elif opcion.upper() == "I":
            estudiante = insertar(estudiante)
            print(estudiante)

        elif opcion.upper() == "E":
            estudiante = editar(estudiante)
            print(estudiante)

        elif opcion.upper() == "D":
            eliminar()

        elif opcion.upper() == "S":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass


if __name__ == "__main__":
    # menu()
    cargar_archivo()
    guardar_archivo("Hola Mundo")
