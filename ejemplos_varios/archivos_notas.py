#! /usr/bin/env python3
# -*- coding: utf-8 -*-


def escribe_archivo(datos):
    reporte = open("reporte.txt", 'w')
    reporte.write(datos)
    reporte.close()


def calcula_promedio(notas):
    promedio = 0
    for i in notas:
        promedio += float(i)
    promedio = promedio / len(notas)
    return promedio


def procesa_archivo(archivo):
    datos = ''
    for linea in archivo:
        cl = linea.strip().split(':')
        nombre = cl[0:1]
        # print(nombre)
        notas = cl[2:len(cl)]
        promedio = calcula_promedio(notas)
        if promedio > 4:
            datos += '{0} {1} , Aprueba\n'.format(nombre[0], promedio)
        else:
            datos += '{0} {1}, Reprueba\n'.format(nombre[0], promedio)
    escribe_archivo(datos)


if __name__ == '__main__':
    nombre_archivo = "notas.txt"
    archivo = open(nombre_archivo, "r")
    procesa_archivo(archivo)
    archivo.close()
