#!/usr/bin/env python3
# -*- coding:utf-8 -*-

def reporte():
    archivo = open("archivo.txt")
    linea=archivo.readline()
    while linea != '':
        proceso = linea.split(":")
        suma = 0
        for i, dato in enumerate(proceso):
            if i > 0:
                suma = suma + float(dato)
            else:
                estudiante = dato

        promedio = suma/6
        if promedio >= 4:
            resultado = "Aprobado"
        else:
            resultado = "Reprobado"

        texto = estudiante + ":" + resultado
        guarda_archivo(texto, "reporte.txt")

        linea=archivo.readline()

        #print(linea)
    archivo.close()

def guarda_archivo(texto, archivo):

    archivo = open(archivo, "a")
    archivo.write(texto)
    archivo.close()
    #json.dump(data, file, indent=4)

def ingreso():
    estudiante = input("Ingrese el Nombre del estudiante: ")

    notas = ""
    for i in range(6):
        # se puede mejorar validando el ingreso
        notas = notas + ":"
        notas = notas + input("Ingrese la nota {0}: ".format(i+1))


    estudiante = estudiante + notas
    guarda_archivo(estudiante, "archivo.txt")

    sn = input("Desea ingresar otro estudiante S/N: ")
    if sn.upper() == 'S':
        ingreso()

if __name__ == '__main__':
    ingreso()
    reporte()
