#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

f = float(input('Ingrese temperatura en Fahrenheit: '))
c = (f - 32.0) * (5.0 / 9.0)
print('El equivalente en Celsius es:', c)
