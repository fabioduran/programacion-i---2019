#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random


def crea_matriz(fila, col):

    """
        Función que crea una matriz de modo alteatorio
        Recibe fila y col tipo entero para el largo de filas y columnas.
        Retorna: Matriz[fila][col]
    """

    # Se usa listas por comprensión para crear la matriz.
    # analice la sintaxis tomando en cuenta los corchetes
    matriz = [
                [random.randint(-10, 10) for x in range(fila)]
                for x in range(col)
            ]

    return matriz


def imprime_matriz(matriz):

    """
        Función que muestra matriz en pantalla.
        Solo se recorre en una dimensión
    """

    for i in matriz:
        print(i)


def menor_elemento(matriz):
    """
        Función que encuentra el elemento menor dentro de una matriz
        Recibe: matriz
    """

    # Valor inicial
    menor = None
    for i, fila in enumerate(matriz):
        for j, col in enumerate(fila):
            # condición que siempre asocia el primer elemento a un valor
            # ademas compara y asocia el valor menor
            if menor is None or menor > col:
                print(menor)
                menor = col
                pos = [i, j]

    print("El número menor es: ", menor)
    print("El número menor se encuentra en las posición: ", pos)


def suma_fila_1_to_5(matriz):
    """
        Función según enunciado, suma los elementos de las primeras 5 filas
        Para esto se usa la técnica de slide para dividir el matriz en el
        rango establecido.
        Recibe: matriz[fila][col]
    """
    print("La suma de los primeros cinco elementos de: ")
    for i, fila in enumerate(matriz):
        suma = 0
        # recorre en slice de la fila
        for col in fila[0:5]:
            suma = suma + col
        print("La fila {0} suma: {1}".format(i, suma))


def cuenta_negativos_5_a_9(matriz):
    """
        Función según enunciado, suma los elementos negativos entre las filas
        5 y 9. Para esto se usa la técnica de slide para dividir el matriz
        en el rango establecido.
        Recibe: matriz[fila][col]
    """
    print("Los números negativos por fila son")
    cuenta = 0
    for i, fila in enumerate(matriz):
        # Slice entre el rango establecido.
        # va entre 5 y 10 para abordar la fila 9
        for col in fila[5:10]:
            if col < 0:
                cuenta = cuenta + 1

    texto = "Entre las filas 5 y 9 "
    texto = texto + "El total de números negativos: {0}".format(cuenta)
    print(texto)


if __name__ == "__main__":
    m = crea_matriz(15, 12)
    imprime_matriz(m)
    menor_elemento(m)
    suma_fila_1_to_5(m)
    cuenta_negativos_5_a_9(m)
