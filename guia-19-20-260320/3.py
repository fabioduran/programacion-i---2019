#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random


def notas_estudiantes(n_estudiantes):
    """
        Función que crea una matriz con estudiantes y sus notas del modo
        [estudiantes, [notas]]
        Todo esto es de modo aleatorio, utilizando listas por comprensión.
        Recibe: entero (numero de estudiantes)
        devuelve: Matriz con todos los elementos.
    """
    estudiantes = []
    for i in range(n_estudiantes):
        # se crea arreglo con notas de forma aleatoria entre 1 y 7
        notas = [round(random.uniform(1, 7), 1)
                 for x in range(random.randint(3, 5))]
        # se agregan los elementos a la matriz, en forma de lista.
        # i+1 refiere a que el rango parte en cero, por lo tanto para nuestra
        # lista sería uno.
        estudiantes.append([i+1, notas])
    return estudiantes


def aprueba_reprueba(estudiante, nota):

    """
        Función que imprime acaso aprueba o reprueba un estudiante según
        condición.
        Recibe:
                * estudiante: el identificador del estudiante
                * nota: nota final.
    """
    if nota >= 4:
        print("El estudiante {0} Aprueba con {1:.1f}".format(estudiante,
                                                             nota))
    else:
        print("El estudiante {0} Reprueba con {1:.1f}".format(estudiante,
                                                              nota))


def recorre_lista_notas(lista_notas):
    """
        Función que recorre la matriz.
        Segun estructura, cada lista al interior de la matriz tiene como primer
        elemento al identificador del estudiante y segundo una lista con todas
        sus notas.
        La imprime tambien la cantidad de aprobados y reprobados.
        Recibe: matriz con todas las notas del curso.
    """
    aprueba = 0
    for i in lista_notas:
        # definición de variables
        suma = 0
        # define variables segun estructura
        estudiante = i[0]
        notas = i[1]
        # recorre todas las notas y obtiene un promedio
        for x in notas:
            suma = suma + x
        promedio = suma / len(notas)
        # llama a función para imprimir
        aprueba_reprueba(estudiante, promedio)
        if promedio >= 4:
            aprueba = aprueba + 1
    # imprime totales de aprobados y reprobados
    print("Aprobaron {0} y reprobaron {1}".format(aprueba,
                                                  len(lista_notas)-aprueba))


if __name__ == "__main__":
    n = 31
    e = notas_estudiantes(n)
    recorre_lista_notas(e)
