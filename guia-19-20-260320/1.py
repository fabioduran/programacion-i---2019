#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random


def tupla_a_lista_tres(tupla):
    """
        Función que transforma una lista a tupla usando slices.
        El primer elemento pasa a ser el último.
    """

    # Selecciona los elementos desde el 1 hasta el final de la tupla
    lista = list(tupla[1:])
    # agrega primer elemento de la tupla a la lista
    lista.append(tupla[0])
    print("La lista generada: ", lista)


def tupla_a_lista_dos(tupla):
    """
        Función que transforma una tupla en lista usando listas por comprensión
        El primer elemento pasa a ser el último
    """

    # Lista por comprensión, agrega todos los elementos de la tupla con
    # la excepción del primer elemento (0)
    lista = [val for x, val in enumerate(tupla) if x != 0]
    lista.append(tupla[0])
    print("La lista generada: ", lista)


def tupla_a_lista_uno(tupla):
    """
        Función que transforma una tupla a lista usando ciclos
    """
    lista = []
    largo = len(tupla)
    # recorre desde el elemento 1 hasta el largo
    for i in range(1, largo):
        lista.append(tupla[i])
    # Primer elemento se agrega al final de la lista
    lista.append(tupla[0])
    print("La lista generada: ", lista)


def crea_tupla():
    """
        Función que genera un tupla de manera aleatoria entre un
        rango de 0 y 20
    """
    largo = random.randint(0, 20)
    # Lista por comprensión que se transforma a tupla
    tupla = tuple([random.randint(0, 10) for x in range(largo)])
    print("La tupla aleatoria generada: ", tupla)

    # Llama a funciones que transforman a lista
    tupla_a_lista_uno(tupla)
    tupla_a_lista_dos(tupla)
    tupla_a_lista_tres(tupla)


if __name__ == "__main__":
    tupla = crea_tupla()
